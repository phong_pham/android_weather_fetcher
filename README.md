# Android Weather Forecast App
##


This repository contains a sample app that implements MVVM clean architecture
[![N|Soid](https://github.com/android10/Sample-Data/raw/master/Android-CleanArchitecture-Kotlin/architecture/clean_architecture_reloaded_layers.png)]
## Technique used in this project:
- Data binding
- View Binding
- DI with Hilt
- LiveData mechanism

## Code Structure

- WeatherApp : Main application class
- Util package: utility classes(currently have one class SecurityUtils for validation root)
- Presentation package: contains view classes and view models
- Domain package: contains use case classes/ entities and repository
- Data package: contains network class/ repository implementaion/ data classes
- Config package: contains constant/ config variables
- Base: contains base class for future use

## Installation

> The app require to install NDK and using the JDK 11 to able to work on local machine

## Checklist marked

- [Programming language:Kotlin is required,Java is optional.] - Kotlin was used on this project
- [Design app'sa rchitecture (suggest MVVM)] - MVVM
- [Apply LiveData mechanism]
- [UI should be looks like in attachment.]
- [Write Unit Tests] - Unit Test in ViewModel/ Repository/ UseCase classes
- [Exception handling] - Handle errors from backend/ network error
- [Caching handling] - Apply caching from rest client with Retrofit. Data cached in view model to avoid reload data in device rotation
- [The application is able to proceed searching with a condition of the search term length
must be from 3 characters or above.] - Condition used in view binding of viewmodel class
- [Secure Android] - Root detection/ Obfuscate code with R8/ Certificate Pinning / Native storing for sensitive string
- Accessibility for Disability Supports: Talk back and scalable text support

