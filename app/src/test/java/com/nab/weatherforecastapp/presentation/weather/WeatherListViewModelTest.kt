package com.nab.weatherforecastapp.presentation.weather

import androidx.lifecycle.asLiveData
import com.nab.weatherforecastapp.data.dto.ErrorResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import com.nab.weatherforecastapp.data.dto.Result
import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import com.nab.weatherforecastapp.presentation.usecase.QueryWeatherUseCase
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nab.weatherforecastapp.presentation.weather.WeatherListViewModel
import com.nab.weatherforecastapp.util.InstantExecutorExtension
import com.nab.weatherforecastapp.util.MainCoroutineRule

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
class WeatherListViewModelTest {
    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Subject under test
    private lateinit var weatherListViewModel: WeatherListViewModel

    private val dataUseCase: QueryWeatherUseCase = mockk()
    val listWeather = listOf(
        WeatherForecastEntity(1L,1.0f, 1.0f, 1.90f,1, "weather 1"),
        WeatherForecastEntity(1L,1.0f, 1.0f, 1.90f,1, "weather 2"))

    @Before
    fun setUp() {
        //1- Mock calls
        coEvery { dataUseCase.invoke("empty") } returns Result.Success(ArrayList())
        coEvery { dataUseCase.invoke("list")} returns Result.Success(listWeather)
        coEvery { dataUseCase.invoke("error") } returns Result.Error(Throwable("error"))
        coEvery { dataUseCase.invoke("businessError") } returns Result.HttpError(ErrorResponse("Error Server", "500"))

    }

    @Test
    fun `get Weather List`() {

        //2-Call
        weatherListViewModel = WeatherListViewModel(dataUseCase)
        weatherListViewModel.queryText.value = "list"
        weatherListViewModel.loadData()
        //active observer for livedata
        weatherListViewModel.listWeatherEntity.asLiveData().observeForever { }

        //3-verify
        val isEmptyList = weatherListViewModel.listWeatherEntity.value.isNullOrEmpty()
        assertEquals(listWeather, weatherListViewModel.listWeatherEntity.value)
        assertEquals(false,isEmptyList)
    }

    @Test
    fun `get Weather Empty List`() {
        //2-Call
        weatherListViewModel = WeatherListViewModel(dataUseCase)
        weatherListViewModel.queryText.value = "empty"
        weatherListViewModel.loadData()
        //active observer for livedata
        weatherListViewModel.listWeatherEntity.asLiveData().observeForever { }

        //3-verify
        val isEmptyList = weatherListViewModel.listWeatherEntity.value.isNullOrEmpty()
        assertEquals(weatherListViewModel.listWeatherEntity.value.size, 0)
        assertEquals(true,isEmptyList)
    }

    @Test
    fun `get Weather Error`() {
        //2-Call
        weatherListViewModel = WeatherListViewModel(dataUseCase)
        weatherListViewModel.queryText.value = "error"
        weatherListViewModel.loadData()
        //active observer for livedata
        weatherListViewModel.listWeatherEntity.asLiveData().observeForever { }

        //3-verify
        val isEmptyList = weatherListViewModel.listWeatherEntity.value.isNullOrEmpty()
        assertEquals(weatherListViewModel.listWeatherEntity.value.size, 0)
        assertEquals(true,isEmptyList)
        assertEquals(weatherListViewModel.errorText.value, "error")
    }

    @Test
    fun `get Weather Server Error`() {
        //2-Call
        weatherListViewModel = WeatherListViewModel(dataUseCase)
        weatherListViewModel.queryText.value = "businessError"
        weatherListViewModel.loadData()
        //active observer for livedata
        weatherListViewModel.listWeatherEntity.asLiveData().observeForever { }

        //3-verify
        val isEmptyList = weatherListViewModel.listWeatherEntity.value.isNullOrEmpty()
        assertEquals(weatherListViewModel.listWeatherEntity.value.size, 0)
        assertEquals(true,isEmptyList)
        assertEquals(weatherListViewModel.errorText.value, "Error Server")
    }

}

