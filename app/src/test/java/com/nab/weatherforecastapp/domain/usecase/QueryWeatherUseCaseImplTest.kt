package com.nab.weatherforecastapp.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nab.weatherforecastapp.data.dto.*
import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import com.nab.weatherforecastapp.domain.repository.WeatherRepository
import com.nab.weatherforecastapp.util.InstantExecutorExtension
import com.nab.weatherforecastapp.util.MainCoroutineRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
class QueryWeatherUseCaseImplTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var useCaseImpl: QueryWeatherUseCaseImpl

    private val repository: WeatherRepository = mockk()

    val listWeather =
        Forecast(listOf(DateForecast(1L, 1L, 1L, TempForecast(1f, 1f, 1f), 1, 1f, listOf()), DateForecast(1L, 1L, 1L, TempForecast(1f, 1f, 1f), 1, 1f, listOf())))

    @Before
    fun setUp() {
        //1- Mock calls
        coEvery { repository.queryWeather("empty") } returns Result.Success(Forecast(listOf()))
        coEvery { repository.queryWeather("list")} returns Result.Success(listWeather)
        coEvery { repository.queryWeather("error") } returns Result.Error(Throwable("error"))
        coEvery { repository.queryWeather("businessError") } returns Result.HttpError(ErrorResponse("Error Server", "500"))

    }
    @Test
    fun `invoke returns list`() {
        //2-Call
        useCaseImpl = QueryWeatherUseCaseImpl(repository)
        val result = runBlocking { useCaseImpl.invoke("list")}

        //3-verify
        assert(result is Result.Success)
        assertEquals((result as Result.Success).data.size, 2)
    }
    @Test
    fun `invoke returns empty`() {
        //2-Call
        useCaseImpl = QueryWeatherUseCaseImpl(repository)
        val result = runBlocking { useCaseImpl.invoke("empty")}

        //3-verify
        assert(result is Result.Success)
        assertEquals((result as Result.Success).data.size, 0)
    }
    @Test
    fun `invoke returns error`() {
        //2-Call
        useCaseImpl = QueryWeatherUseCaseImpl(repository)
        val result = runBlocking { useCaseImpl.invoke("error")}

        //3-verify
        assert(result is Result.Error)
        assertEquals((result as Result.Error).throwable.message, "error")
    }

    @Test
    fun `invoke returns error code`() {
        //2-Call
        useCaseImpl = QueryWeatherUseCaseImpl(repository)
        val result = runBlocking { useCaseImpl.invoke("businessError")}

        //3-verify
        assert(result is Result.HttpError)
        assertEquals((result as Result.HttpError).response.message, "Error Server")
        assertEquals((result as Result.HttpError).response.cod, "500")
    }
}