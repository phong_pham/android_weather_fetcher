package com.nab.weatherforecastapp.data

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import com.nab.weatherforecastapp.config.Config
import com.nab.weatherforecastapp.config.IConfig
import com.nab.weatherforecastapp.data.dto.*
import com.nab.weatherforecastapp.util.InstantExecutorExtension
import com.nab.weatherforecastapp.util.MainCoroutineRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import retrofit2.HttpException
import retrofit2.Response

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
class WeatherRepositoryImplTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var repositoryImpl: WeatherRepositoryImpl

    private val mockService: WeatherService = mockk()
    private val mockContext: Context = mockk()
    private val mockConfig: IConfig = mockk()
    val listWeather =
        Forecast(listOf(DateForecast(1L, 1L, 1L, TempForecast(1f, 1f, 1f), 1, 1f, listOf()), DateForecast(1L, 1L, 1L, TempForecast(1f, 1f, 1f), 1, 1f, listOf())))

    @Before
    fun setUp() {
        //1- Mock calls
        coEvery { mockConfig.getAppId() } returns ""
        coEvery { mockService.getForecast(cityName= "empty", appId= any()) } returns Forecast(listOf())
        coEvery { mockService.getForecast(cityName="list", appId= any())} returns listWeather
        coEvery { mockService.getForecast(cityName="error", appId= any()) } throws Exception("error")
        coEvery { mockService.getForecast(cityName="businessError", appId= any()) } throws HttpException(Response.error<ErrorResponse>(500, ResponseBody.Companion.create(
            "application/json".toMediaType(), Gson().toJson(ErrorResponse("errorHTTP","123")).toString())))
    }
    @Test
    fun `invoke returns list`() {
        //2-Call
        repositoryImpl = WeatherRepositoryImpl(mockContext, mockConfig, mockService)
        val result = runBlocking { repositoryImpl.queryWeather("list")}

        //3-verify
        assert(result is Result.Success)
        assertEquals((result as Result.Success).data, listWeather)
    }
    @Test
    fun `invoke returns empty`() {
        //2-Call
        repositoryImpl = WeatherRepositoryImpl(mockContext, mockConfig, mockService)
        val result = runBlocking { repositoryImpl.queryWeather("empty")}

        //3-verify
        assert(result is Result.Success)
        assertEquals((result as Result.Success).data.list.size, 0)
    }
    @Test
    fun `invoke returns error`() {
        //2-Call
        repositoryImpl = WeatherRepositoryImpl(mockContext, mockConfig, mockService)
        val result = runBlocking { repositoryImpl.queryWeather("error")}

        //3-verify
        assert(result is Result.Error)
        assertEquals((result as Result.Error).throwable.message, "error")
    }

    @Test
    fun `invoke returns error code`() {
        //2-Call
        repositoryImpl = WeatherRepositoryImpl(mockContext, mockConfig, mockService)
        val result = runBlocking { repositoryImpl.queryWeather("businessError")}

        //3-verify
        assert(result is Result.HttpError)
        assertEquals((result as Result.HttpError).response.message, "errorHTTP")
    }
}