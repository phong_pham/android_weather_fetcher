package com.nab.weatherforecastapp.domain.usecase

import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import com.nab.weatherforecastapp.domain.repository.WeatherRepository
import com.nab.weatherforecastapp.presentation.usecase.QueryWeatherUseCase
import javax.inject.Inject
import com.nab.weatherforecastapp.data.dto.Result
import com.nab.weatherforecastapp.domain.Mapper

class QueryWeatherUseCaseImpl @Inject constructor(
    private val repository: WeatherRepository
) : QueryWeatherUseCase {

    override suspend fun invoke(city: String): Result<List<WeatherForecastEntity>> {
        return when (val result = repository.queryWeather(city)) {
            is Result.Success -> Result.Success(result.data.list.map { Mapper.map(it)})
            is Result.Error -> Result.Error(result.throwable)
            is Result.HttpError -> Result.HttpError(result.response)
        }

    }
}
