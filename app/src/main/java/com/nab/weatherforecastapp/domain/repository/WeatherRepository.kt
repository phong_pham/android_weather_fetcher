package com.nab.weatherforecastapp.domain.repository

import com.nab.weatherforecastapp.data.dto.Forecast
import com.nab.weatherforecastapp.data.dto.Result
interface WeatherRepository {

    suspend fun queryWeather(city: String): Result<Forecast>
}
