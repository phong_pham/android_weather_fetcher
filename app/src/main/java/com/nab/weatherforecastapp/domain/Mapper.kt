package com.nab.weatherforecastapp.domain

import com.nab.weatherforecastapp.data.dto.DateForecast
import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity

object Mapper {

    fun map(dateDto: DateForecast) = WeatherForecastEntity(
        dateDto.dt * 1000,
        dateDto.temp.max,
        dateDto.temp.min,
        dateDto.pressure,
        dateDto.humidity,
        dateDto.weather.firstOrNull()?.description ?: ""
    )
}