package com.nab.weatherforecastapp.domain.entity

data class WeatherForecastEntity(
    val dateTime: Long,
    val tempMax: Float,
    val tempMin: Float,
    val pressure: Float,
    val humidity: Int,
    val description: String
)

