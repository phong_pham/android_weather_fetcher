package com.nab.weatherforecastapp.presentation.usecase

import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import com.nab.weatherforecastapp.data.dto.Result

interface QueryWeatherUseCase {

    suspend operator fun invoke(city: String): Result<List<WeatherForecastEntity>>
}
