package com.nab.weatherforecastapp.presentation.weather

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.nab.weatherforecastapp.R
import com.nab.weatherforecastapp.BR
import com.nab.weatherforecastapp.base.BaseFragment
import com.nab.weatherforecastapp.databinding.FragmentWeatherListBinding
import com.nab.weatherforecastapp.presentation.weather.adapters.WeatherAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class WeatherListFragment :
    BaseFragment<WeatherListViewModel, FragmentWeatherListBinding>() {

    private val viewModel by viewModels<WeatherListViewModel>()
    private val rvAdapter by lazy {
        WeatherAdapter(
        )
    }

    override fun getLayoutId() = R.layout.fragment_weather_list

    override fun getBindingViewModelId() = BR.view_model

    override fun getAssociatedViewModel() = viewModel

    override fun initView() {
        binding.rvWeather.apply {
            val layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setLayoutManager(layoutManager)
            setHasFixedSize(true)
            adapter = rvAdapter
        }
    }

    override fun observeDataChanged() {
        with(viewModel) {
            listWeatherEntity.onEach {
                rvAdapter.update(it)
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    }


