package com.nab.weatherforecastapp.presentation.weather.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nab.weatherforecastapp.R
import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import java.text.SimpleDateFormat
import java.util.*

class WeatherAdapter :
    RecyclerView.Adapter<WeatherAdapter.MViewHolder>() {
    private var weathers: List<WeatherForecastEntity> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_weather, parent, false)
        return MViewHolder(view)
    }

    override fun onBindViewHolder(vh: MViewHolder, position: Int) {
        vh.bind(weathers[position])
    }

    override fun getItemCount(): Int {
        return weathers.size
    }

    fun update(data: List<WeatherForecastEntity>) {
        weathers = data
        notifyDataSetChanged()
    }

    class MViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val date_time = view.findViewById<TextView>(R.id.date_time)
        private val average_temperature = view.findViewById<TextView>(R.id.average_temperature)
        private val pressure = view.findViewById<TextView>(R.id.pressure)
        private val humidity = view.findViewById<TextView>(R.id.humidity)
        private val description = view.findViewById<TextView>(R.id.description)
        fun bind(dateInfo: WeatherForecastEntity) {
            val sdf = SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH)
            val date = Date(dateInfo.dateTime )
            date_time.text = sdf.format(date)

            val averageTemp = (dateInfo.tempMax + dateInfo.tempMin) / 2
            val solution: Double = String.format("%.0f", averageTemp).toDouble()
            average_temperature.text = "${solution}°C"

            pressure.text = dateInfo.pressure.toString()
            humidity.text = "${dateInfo.humidity}%"
            description.text = dateInfo.description
        }
    }
}