package com.nab.weatherforecastapp.presentation.main

import com.nab.weatherforecastapp.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor() : BaseViewModel()
