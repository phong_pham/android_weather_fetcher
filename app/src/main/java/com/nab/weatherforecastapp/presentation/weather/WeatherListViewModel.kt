package com.nab.weatherforecastapp.presentation.weather

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nab.weatherforecastapp.base.BaseViewModel
import com.nab.weatherforecastapp.data.dto.Result
import com.nab.weatherforecastapp.domain.entity.WeatherForecastEntity
import com.nab.weatherforecastapp.presentation.usecase.QueryWeatherUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherListViewModel @Inject constructor(
    private val queryWeatherUseCase: QueryWeatherUseCase
) : BaseViewModel() {
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val _listWeatherEntity: MutableStateFlow<List<WeatherForecastEntity>> by lazy {
        MutableStateFlow(mutableListOf())
    }
    val listWeatherEntity = _listWeatherEntity.asStateFlow()

    val queryText = MutableLiveData<String>()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val _errorText: MutableStateFlow<String?> by lazy {
        MutableStateFlow(null)
    }
    val errorText = _errorText.asStateFlow()

    fun loadData() {
        viewModelScope.launch {
            _errorText.emit(null)
            queryText.value?.let { it1 ->
                showLoading()
                queryWeatherUseCase(it1).let {
                    when (val result = it) {
                        is Result.Success -> _listWeatherEntity.emit(result.data)
                        is Result.Error -> _errorText.emit(result.throwable.message)
                        is Result.HttpError -> _errorText.emit(result.response.message)
                    }
                }
                hideLoading()
            }
        }
    }

}
