package com.nab.weatherforecastapp.presentation.main

import androidx.activity.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.nab.weatherforecastapp.R
import com.nab.weatherforecastapp.base.BaseActivity
import com.nab.weatherforecastapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<MainActivityViewModel, ActivityMainBinding>() {

    private val viewModel by viewModels<MainActivityViewModel>()

    private val navController by lazy {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_main) as NavHostFragment
        navHostFragment.navController
    }

    override fun getLayoutId() = R.layout.activity_main

    override fun getAssociatedViewModel() = viewModel

    override fun observeDataChanged() {
    }
}
