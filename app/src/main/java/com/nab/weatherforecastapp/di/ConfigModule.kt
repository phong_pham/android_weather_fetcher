package com.nab.weatherforecastapp.di

import com.nab.weatherforecastapp.config.Config
import com.nab.weatherforecastapp.config.IConfig
import com.nab.weatherforecastapp.domain.repository.WeatherRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ConfigModule {

    @Provides
    @Singleton
    fun provideIConfig(): IConfig {return Config()}
}
