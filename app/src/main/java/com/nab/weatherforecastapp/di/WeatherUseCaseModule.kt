package com.nab.weatherforecastapp.di

import com.nab.weatherforecastapp.domain.usecase.QueryWeatherUseCaseImpl
import com.nab.weatherforecastapp.presentation.usecase.QueryWeatherUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class WeatherUseCaseModule {

    @Binds
    abstract fun provideQueryWeatherUseCase(
        getQueryWeatherUseCaseImpl: QueryWeatherUseCaseImpl
    ): QueryWeatherUseCase
}
