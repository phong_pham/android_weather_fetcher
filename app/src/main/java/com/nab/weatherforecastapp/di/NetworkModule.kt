package com.nab.weatherforecastapp.di

import android.content.Context
import android.content.pm.ApplicationInfo
import android.net.ConnectivityManager
import android.os.Build
import com.nab.weatherforecastapp.config.Constant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val TIMEOUT = 60L

    @Provides
    @Singleton
    fun provideLoggingInterceptor(@ApplicationContext appContext: Context): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()

        logging.level =
            if (isDebuggable(appContext)) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        return logging
    }
    @Provides
    @Singleton
    fun provideCache(@ApplicationContext appContext: Context): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(appContext?.cacheDir!!, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideOfflineInterceptor(@ApplicationContext appContext: Context): Interceptor
    {
        return Interceptor { chain ->
            var request: Request = chain.request()
            val maxAge = 60 // read from cache for 60 seconds even if there is internet connection
            request.newBuilder()
                .header("Cache-Control", "public, max-age=$maxAge")
                .removeHeader("Pragma")
                .build()
            if (!isInternetConnected(appContext)) {
                val maxStale = 60 * 60 // available in 1 hour
                request = request.newBuilder()
                    .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                    .removeHeader("Pragma")
                    .build()
            }
            chain.proceed(request)
        }
    }
    @Provides
    @Singleton
    fun provideDispatcher(): Dispatcher {
        val dispatcher = Dispatcher().apply {
            maxRequests = 1
        }
        return dispatcher
    }

    @Provides
    @Singleton
    fun provideConnectionSpecs(): ConnectionSpec {
        return ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            .tlsVersions(TlsVersion.TLS_1_2)
            .cipherSuites(
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256
            )
            .build()
    }

    @Provides
    @Suppress("LongParameterList")
    fun provideOkHttpClientBuilder(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        dispatcher: Dispatcher,
        connectionSpec: ConnectionSpec,
        cache: Cache,
        onlineInterceptor : Interceptor,
        offlineInterceptor : Interceptor
    ): OkHttpClient.Builder {
        return OkHttpClient().newBuilder()
            .dispatcher(dispatcher)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .connectionSpecs(arrayListOf(connectionSpec, ConnectionSpec.CLEARTEXT)).cache(cache)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(offlineInterceptor)
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun provideOkHttpClient(okHttpClientBuilder: OkHttpClient.Builder): OkHttpClient {
        return okHttpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return buildRetrofit(gsonConverterFactory, okHttpClient)
    }

    private fun isDebuggable(appContext: Context) =
        0 != appContext.packageManager?.getApplicationInfo(
            appContext.packageName!!,
            0
        )?.flags?.and(ApplicationInfo.FLAG_DEBUGGABLE) ?: false

    private fun buildRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constant.API_BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .client(okHttpClient)
            .build()
    }

    fun isInternetConnected(getApplicationContext: Context?): Boolean {
        getApplicationContext?: return false
        var status = false
        val cm =
            getApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm.activeNetwork != null && cm.getNetworkCapabilities(cm.activeNetwork) != null) {
                // connected to the internet
                status = true
            }
        } else {
            if (cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting) {
                // connected to the internet
                status = true
            }
        }
        return status
    }
}
