package com.nab.weatherforecastapp.di

import com.nab.weatherforecastapp.data.WeatherRepositoryImpl
import com.nab.weatherforecastapp.domain.repository.WeatherRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class WeatherRepositoryModule {

    @Binds
    @Singleton
    abstract fun provideRepository(timesheetRepositoryImpl: WeatherRepositoryImpl): WeatherRepository
}
