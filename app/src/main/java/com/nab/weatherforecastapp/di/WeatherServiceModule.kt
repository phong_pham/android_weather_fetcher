package com.nab.weatherforecastapp.di

import com.nab.weatherforecastapp.data.WeatherService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object WeatherServiceModule {

    @Provides
    @Singleton
    fun provideService(retrofit: Retrofit) = retrofit.create(WeatherService::class.java)
}
