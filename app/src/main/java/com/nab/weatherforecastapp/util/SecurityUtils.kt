package com.nab.weatherforecastapp.util
import com.nab.weatherforecastapp.config.Config

class SecurityUtils() {
    private var isRootAvailable = false

    @Throws(ValidationException::class)
    fun validateAllOrThrowException() {
        validateRoot()
    }

    @Throws(ValidationException::class)
    private fun validateRoot() {
        if (isRootAvailable) return  // // validation success (no validation need)

        if (Config().isRoot()) throw ValidationException(
            ValidationException.ERROR_CODE_ROOT_EXCEPTION,
            "Run in root mode"
        )

    }


    class ValidationException : Exception {

        val errorCode: Int

        constructor(code: Int, message: String?) : super(message) {
            errorCode = code
        }

        constructor(code: Int, message: String?, cause: Throwable?) : super(message, cause) {
            errorCode = code
        }

        companion object {
            val ERROR_CODE_ROOT_EXCEPTION = 3
        }
    }

}
