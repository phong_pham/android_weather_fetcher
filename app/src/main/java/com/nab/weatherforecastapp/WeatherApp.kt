package com.nab.weatherforecastapp

import android.app.Application
import com.nab.weatherforecastapp.util.SecurityUtils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()
        SecurityUtils().validateAllOrThrowException()
    }
}