package com.nab.weatherforecastapp.config

interface IConfig {
    fun getAppId() : String
    fun Statfile() : Boolean
    fun isRoot(): Boolean
}