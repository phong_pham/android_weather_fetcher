package com.nab.weatherforecastapp.config

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

class Config : IConfig {

        init {
            System.loadLibrary("native")
        }
        external override fun getAppId() : String
        external override fun Statfile() : Boolean

    override fun isRoot(): Boolean {
        return try {
            Statfile()
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

}