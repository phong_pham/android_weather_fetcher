package com.nab.weatherforecastapp.config

class Constant {
    companion object {
        const val API_BASE_URL = "https://api.openweathermap.org/"
    }
}