package com.nab.weatherforecastapp.data.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Forecast(
    val list: List<DateForecast>
) : Parcelable