package com.nab.weatherforecastapp.data.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Parcelize
@Keep
data class TempForecast(
    val day: Float,
    val min: Float,
    val max: Float
) : Parcelable