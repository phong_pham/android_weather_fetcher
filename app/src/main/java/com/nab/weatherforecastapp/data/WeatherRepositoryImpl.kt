package com.nab.weatherforecastapp.data

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nab.weatherforecastapp.config.Config
import com.nab.weatherforecastapp.config.IConfig
import com.nab.weatherforecastapp.data.dto.ErrorResponse
import com.nab.weatherforecastapp.data.dto.Forecast
import com.nab.weatherforecastapp.domain.repository.WeatherRepository
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import com.nab.weatherforecastapp.data.dto.Result
import retrofit2.HttpException

class WeatherRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val config: IConfig,
    private val service: WeatherService
) : WeatherRepository {

    override suspend fun queryWeather(city: String): Result<Forecast>{
        return try {
            val value = service.getForecast(cityName = city, appId = config.getAppId())
            Result.Success(value)
        } catch (e: Throwable) {
            if(e is HttpException){
                val response = e.response()
                val gson = Gson()
                val type = object : TypeToken<ErrorResponse>() {}.type
                val errorResponse: ErrorResponse = gson.fromJson(response?.errorBody()!!.charStream(), type)
                Result.HttpError(errorResponse)
            } else
            Result.Error(e)
        }
    }
}
