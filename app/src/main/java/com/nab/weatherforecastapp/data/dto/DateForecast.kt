package com.nab.weatherforecastapp.data.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Parcelize
@Keep
data class DateForecast(
    val dt: Long,
    val sunrise: Long,
    val sunset: Long,
    val temp: TempForecast,
    val humidity: Int,
    val pressure: Float,
    val weather: List<WeatherCondition>
) : Parcelable