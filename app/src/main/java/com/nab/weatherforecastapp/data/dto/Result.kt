package com.nab.weatherforecastapp.data.dto
import com.nab.weatherforecastapp.data.dto.ErrorResponse
sealed class Result<out T: Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val throwable: Throwable) : Result<Nothing>()
    data class HttpError(val response: ErrorResponse) : Result<Nothing>()
}
