package com.nab.weatherforecastapp.data.dto

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Parcelize
@Keep
data class WeatherCondition(
    val main: String,
    val icon: String,
    val description: String
) : Parcelable