package com.nab.weatherforecastapp.data

import com.nab.weatherforecastapp.data.dto.Forecast
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("data/2.5/forecast/daily")
    suspend fun getForecast(
        @Query("q") cityName: String,
        @Query("units") units: String = "imperial",
        @Query("appid") appId: String,
        @Query("cnt") cnt: Int = 7
    ): Forecast
}
