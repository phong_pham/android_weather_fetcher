#include "native.h"
#include "native.h"
#include <jni.h>
#include <string>
#include <android/log.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

#define DEBUG_TAG "WeatherApp"

#include <stdbool.h>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_nab_weatherforecastapp_config_Config_getAppId(JNIEnv *env, jobject thiz) {
    std::string JNI_API_KEY = API_KEY_ID;
    return env->NewStringUTF(JNI_API_KEY.c_str());
}



extern "C"
JNIEXPORT jboolean JNICALL
Java_com_nab_weatherforecastapp_config_Config_Statfile(JNIEnv *env, jobject thiz) {
    const char *strings[] = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su",
                             "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su",
                             "/system/sd/xbin/su",
                             "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
    size_t i = 0;
    for (i = 0; i < sizeof(strings) / sizeof(strings[0]); i++) {
        const char *path = strings[i];
        struct stat fileattrib;
        if (stat(path, &fileattrib) < 0) {
            __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: stat error: [%s][%s]",
                                strerror(errno), path);
        } else {
            __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG,
                                "NATIVE: stat success, access perms: [%d]", fileattrib.st_mode);
            return 1;
        }
    }
    jboolean run_su_success = 1;
    jboolean isCopy;
    int status;
    pid_t pid = -1;
    pid = fork();
    if (pid == 0) {
// Child process will return 0 from fork()
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: Child process here.");
        int result = execl("/system/xbin/su", "su", NULL);
        int errsv = errno;
        if (result != 0) {
            char buffer[256];
            char *errorMessage = reinterpret_cast<char *>(strerror_r(errsv, buffer, 256)); // get string message from errno
            __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: execl error: [%s]",
                                errorMessage);
            return 0;

        } else {
            __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: execl no error");
            return 1;
        }
    } else if (pid > 0) {
// Parent process will return a non-zero value from fork()
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: Parent process here.");
        wait(&status);
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: Parent done waiting.");
    } else {
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: runsu: error with fork()\n");
        status = 1;
    }

    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: status: [%d]", status);
    if (status) {
        run_su_success = 0;
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: Error in running command.");
    } else {
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NATIVE: Success in running command.");
    }

    return run_su_success;
}